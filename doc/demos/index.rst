.. _demos:

Cookbook guides
===============

Contents:

.. toctree::
   :maxdepth: 4
   :glob:

   inference/inference
   reconstruction_direct/reconstruction
   reconstruction_indirect/reconstruction
   animation/animation
   matplotlib/matplotlib
   cppextensions/cppextensions
